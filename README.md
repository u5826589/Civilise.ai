# ![civilise logo](resources/Artboard_1ldpi.png)
### AI for a better connected, more equitable world. 

Table of Contents
=================

  * [Introduction](#introduction)
  * [Customer Understanding](#customer-understanding)
  * [Team](#team)
  * [System](#system)
      * [Eagle - Building Detection](#eagle-building-detection)
      * [Crystal - Population and Settlement Prediction](#crystal-population-and-settlement-prediction)
      * [Oak - Network Design &amp; Path Planning](#oak-network-design-path-planning)
  * [Progress and Planning](#progress-and-planning)
  * [Showcase](#showcase)
  * [Outcome](#outcome)
  * [Next steps](#next-steps)
  * [Linkdump](#linkdump)
  * [Access](#access)

Created by [gh-md-toc](https://github.com/ekalinin/github-markdown-toc)

## Introduction
Civilise.ai is a system to produce efficient infrastructure network designs for low and middle income countries (LMICs). Work on the project began on March 4th, 2017. Documentation and code is in our GitLab [repositories](https://gitlab.com/civilise-ai) and we collaborate online via [slack](https://civilise-ai.slack.com). To gain access to any of these platforms please email John Forbes through john.forbes@civilise.ai. For further details on our current stage of progress there is an audit report and presentation found in this repository here ([Audit](Audits) folder).

## Customer Understanding
This project is based on a hypothesis grounded by the large scale global demographic trends of population growth and urbanisation. The core of this hypothesis is that local municipal governments will require assistance to upgrade their settlements economically and rapidly to accommodate meet projected urban growth.

To validate this hypothesis our team has engaged with stakeholders, potential customers, and potential partners. We will continue to build relationships and explore possible value propositions.

On June 2nd we engaged with a team within the ACT Economic Development staff to validate and inform our ongoing development. We have since engaged with both Geospatial Intelligence Pty Ltd, and Purdon Planning in the ACT to showcase our technology, and interact with the Canberra community. 
 
## Team
Our team meets regularly in person on Mondays for tutorials and Saturdays for group work. 

Team roles are defined as follows:

| Team Member     | Dev Role      | Key Skills                            | Canvas Domain          |
| --------------- | ------------- | ------------------------------------- | ---------------------- |
| John Forbes     | Oak           | AI, Engineering, Management           | Value Propositions     |
| Sebastian Lau   | Oak           | Data mining, AI, Python, Java         | Channels               |
| Jonathon Martin | Crystal       | AI; Python; Java; Ionic               | Customers/Partners     |
| Ted Pettigrove  | Project Manager         | AI, Coding, Leadership, Communication | Cost/Revenue Structure |
| Darren Li       | Crystal | Coding, Network Engineering           | Key Resources          |


## System
The Civilise.ai system is comprised of three components, Eagle (building detection), Crystal (population and settlement prediction) and Oak (network design & path planning). 

![eagle logo](resources/eagle-icon.png) ![crystal logo](resources/crystals-icon.png) ![oak logo](resources/oak-icon.png) 

![system diagram](resources/system.png)

### Eagle - Building Detection
Eagle uses computer vision to identify features in satellite imagery to establish ground truth. It detects and outputs the locations of buildings and other demand points (location requiring a network service provision) from aerial images. This data is shared with the other systems to analyse the area and output useful data for infrastructure planning.

This subsystem has been on hold for this semester as Civilise.ai has lost the team member responsible for computer vision, and we have also acquired street addresse and road data from alternative sources.

### Crystal - Population and Settlement Prediction
Crystal is the settlement growth prediction element of the Civilise.ai system. It uses unsupervised machine learning technologies to predict the future state of human population distribution using global datasets produced and maintained by the Center for International Earth Science Information Network (CIESIN) and the NASA Socioeconomic Data and Applications Center (SEDAC).

### Oak - Network Design & Path Planning
The Oak subsystem in responsible for producing an efficient multi-layer network design - the final output of the Civilise.ai system. Possible infrastructure layers are: water, telecommunications, electricity. Oak will also produce a construction schedule and bill of materials for each design. For the purposes of developing a prototype for techlauncher, specific infrastructure details were omitted.
Using this plan, our customers will be able to deploy infrastructure cost effectively and time effectively.

## Progress and Planning

![Timeline](resources/Timeline.png)

This semester we have lost a member of our team and therefore we have changed scope and now defined our MVP, to be completed by the end of the year. Eagle will holt development but will be sustained and maintained to integrate into the MVP. By the end of semester, Crystal will produce usable future predictions for selected regions to inform Oak. Oaks deliverables for the end of semester include having a baseline solver, integrated performance measures and an input interface. This will all allow for our MVP to be a single layer infrastructure design plan. 

## Showcase
We had a huge success at the showcase and have attracted a lot of interest. We met a lot of potential stakeholders and collected industry business cards.

The following is our poster we used for the showcase.

![poster small](resources/poster_small.png)

## Outcome

For this semester, we have achieved our goal of producing a MVP of our services. The three subsystems are integrated, and individually they are capable of performing the required functionality. Eagle, through either feature detection or searching through existing database, can output the required geological data; Crystal predicts future population growth for an area and output the corresponding demands; and Oak uses these data to perform basic network solving and produce a simple infrastructure design.

The next steps for the subsystems are:

+ Eagle: either acquiring national databases on geological data or improving on our current feature detection system
+ Crystal: improving on existing neural network model as to increase its capabilities, train on more data and thus improve its accuracy and efficiency
+ Oak: Further incorporating engineering rules and real life constraints into the test/simulation system, and iteratively improving on the accuracy and efficiency of the solver

In the next year, Civilise.ai will aim to evolve from a techlauncher project into a startup business with external fundings.

## Next steps
We will be returning to Techlauncher next year with a real world problem to tackle. The techlauncher project next year will be driven by real world customer needs as defined by the company.  This future activity will apply and refine the system to improve its commercial value.


## Linkdump
#### [Audit report](Audits/Project\ Audit\ 2\ Report.pdf)
#### [Audit slides](Audits/Project\ Audit\ 2\ Slides.pdf)
#### [Demo videos](videos)
#### [Showcase poster](Audits/showcase_poster.pdf)
#### [Repos](https://gitlab.com/civilise-ai)
#### [Meeting minutes](https://gitlab.com/civilise-ai/hive/tree/master/minutes)
#### [Docker link](https://hub.docker.com/r/jonathon3395/crystal/)

## Access
You may ask us (via email, etc) to give access permission to your own Gitlab.com account or use the following dummy account to access the repositories.

Please adhere to the following guidelines when using the dummy account:
- Do **NOT** use the dummy account for purposes other than accessing the Civilise.ai repositories.
- Do **NOT** attach any personal/extra information to the dummy account, this includes filling in personal information, personal email addresses, linking to accounts on other sites.
- Do **NOT** modify the account attributes of the dummy account, this includes email address, account password.

```
user name : civdummy2
password  : h2(,3P~Jfgda=PKY^U+BN!:Pcsn_`~
```
